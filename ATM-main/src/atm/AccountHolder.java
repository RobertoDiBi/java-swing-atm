package atm;

/**
 *
 * @author 1796128  - Roberto Di Biase - ATM project
 */
public class AccountHolder extends Bank{
    
    private String accountHolderName;

    public AccountHolder(String accountHolderName, String bankName) {
        super(bankName);
        this.accountHolderName = accountHolderName;
    }
    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }
}