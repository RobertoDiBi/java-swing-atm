package atm;

/**
 *
 * @author 1796128  - Roberto Di Biase - ATM project
 */
public class Account extends AccountHolder{
    
    private int accountNumber;
    private double balance;
    private int withdrawalLimit;

    public Account(int accountNumber, double balance, int withdrawalLimit, String accountHolderName, String bankName) {
        super(accountHolderName, bankName);
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.withdrawalLimit = withdrawalLimit;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public int getWithdrawalLimit() {
        return withdrawalLimit;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setWithdrawalLimit(int withdrawalLimit) {
        this.withdrawalLimit = withdrawalLimit;
    }
    
    /**Check the balance when the user wants to withdrawal money. To ensure that on the amount is not 0$
     * 
     * @return 
     */   
    public boolean checkBalance(){
        boolean check=true;
        if (this.balance==0)
            check=false;

        return check;      
        }
}
