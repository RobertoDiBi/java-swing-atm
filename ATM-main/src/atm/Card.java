package atm;

import javax.swing.JOptionPane;

/**
 *
 * @author 1796128  - Roberto Di Biase - ATM project
 */
public class Card extends Account{
    
    private int cardNumber;
    private int PIN;

    public Card(int cardNumber, int PIN, int accountNumber, double balance, int withdrawalLimit, String accountHolderName, String bankName) {
        super(accountNumber, balance, withdrawalLimit, accountHolderName, bankName);
        this.cardNumber = cardNumber;
        this.PIN = PIN;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getPIN() {
        return PIN;
    }

    public void setPIN(int PIN) {
        this.PIN = PIN;
    }
    
    /**Method to check that the PIN entered by the user correspond to the one in the file and gives a message with the number of attempt. 
     * It also reads the file and saves all information in an array
     * 
     * @param pin
     * @param attempt
     * @return 
     */ 
    public boolean checkPin(int pin, int attempt){
        boolean check=false;
         
        if (pin==PIN)
            check=true;
        else if (attempt<=2){
            JOptionPane.showMessageDialog(null,"Incorrect PIN.\nAttempt: " + attempt +"/3","ERROR", JOptionPane.WARNING_MESSAGE);
            int result = JOptionPane.showConfirmDialog(null,
                    "Do you want to enter it again?",
                    "Confirm Quit",JOptionPane.YES_NO_OPTION);
            if (result == 1) System.exit(0);
        }
        return check;
    }  
}