package atm;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;

/**
 *
 * @author 1796128  - Roberto Di Biase - ATM project
 */
public class ATMMain {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException{
        
        //Declaring variables
        String filename,//Holds the Card number insert by the user 
                tempPIN;
        int answer=0,//Holds the answer to perform another transaction 
                logoutAnswer;//Holds the answer to logout after the user doesn't want to do another transaction
        int cardNumber=0;
        boolean check,//Hold the pinCheck value returned by pinCheck method
                check2;//Check if the pin corresponds to an integer
        int attempt=0,//Counts how many time the user insert an incorrect PIN
              pin,//Holds the pin insert by the user
               menuSelection;//Holds the menuSelection value from the Optionpane
        
        do{
                filename =JOptionPane.showInputDialog(null,"Enter Card number:");
                if (filename==null)
                    System.exit(0);
                try{
                cardNumber = Integer.parseInt(filename);
                 }catch(NumberFormatException e){
                JOptionPane.showMessageDialog(null,"Please enter digits only!","ERROR", JOptionPane.WARNING_MESSAGE);
                }
                filename = filename + ".txt";
                check=checkCard(filename); 
        }while(!check);
        
        Card card = new Card(cardNumber, 0, 0, 0, 0, "","TOP BANK");
        //Reading the file and saving the account information 
        try(FileReader freader = new FileReader(filename)){
            BufferedReader inputFile = new BufferedReader(freader);
            boolean done=false;
            while (!done){

                card.setPIN(Integer.parseInt(inputFile.readLine()));
                card.setAccountHolderName(inputFile.readLine());
                card.setBalance(Double.parseDouble(inputFile.readLine()));
                card.setWithdrawalLimit(Integer.parseInt(inputFile.readLine()));
                card.setAccountNumber(Integer.parseInt(inputFile.readLine()));
                
                done=true;
            }
        }
        
        //Loop that verify the PIN insert from the user corresponds to the one read in the file
        do{
            attempt++;
            if (attempt>3)
            {
                JOptionPane.showMessageDialog(null,"ATTEMPT: "+ (attempt-=1) +"/3\n"+"CARD LOCKED! Inccorect access code.\nPlease go to one of our branches to: \n-VERIFY your identity\n-UNLOCK your card ","ACCESS DENIED", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }
            do{
                try{
                    tempPIN=JOptionPane.showInputDialog(null,"Please enter your PIN");
                    if (tempPIN==null)
                        System.exit(0);
                    pin=Integer.parseInt(tempPIN);
                    check=card.checkPin(pin,attempt);
                    check2=true;
                }catch (NumberFormatException nfe){
                    JOptionPane.showMessageDialog(null,"Please digits only","Incorrect input", JOptionPane.WARNING_MESSAGE);
                    check2=false;
                }
            }while(!check2);
        }while(!check);
        
        ATM atm = new ATM(card.getBankName());
        //Display to select the different transaction
        while(answer==0){
        Object[] options = {"Logout","Change PIN","Balance","Deposit","Withdrawal"};
                menuSelection = JOptionPane.showOptionDialog(null,
                ("Welcome, "+card.getAccountHolderName()+ "\nSelect a transaction"),
                "TOP BANK ATM",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options);
                
        switch(menuSelection){
            case 4:
                atm.withdrawal(card);//Calling the withdrawal method to perform a withdrawal
                
            break;
            case 3:
                atm.deposit(card);//Calling the deposit method to perform a deposit
                atm.printReceipt(card);//Calling the printReceipt to ask the user if he wants one
            break;
            case 2:
                //Printing the name and account balance
                JOptionPane.showMessageDialog(null,card.getBankName()+"\nAccount No: "+ card.getAccountNumber()+"\n"+ card.getAccountHolderName()+"\nYour balance is: $" + card.getBalance());
            break;
            case 1:
                atm.newPIN(card);//Calling newPIN method to cange PIN
            break;
            case 0:
                logoutAnswer =JOptionPane.showConfirmDialog(null,"Are you sure you want to logout?","TOP BANK ATM",JOptionPane.YES_NO_OPTION);
                if (logoutAnswer==0){
                    JOptionPane.showMessageDialog(null,"Thank you, "+ card.getAccountHolderName()+ "\nfor using TOP BANK ATM.", "Logout successfully.",JOptionPane.INFORMATION_MESSAGE);
                    updateFile(card, filename);//updating the file using updateFile method
                    System.exit(0);//quit program
                }
            break;
        }
        
        //More transactions
             answer=JOptionPane.showConfirmDialog(null,
                     "Do you want to perform another transaction?","TOP BANK ATM",JOptionPane.YES_NO_OPTION);
            if(answer==1){
                    JOptionPane.showMessageDialog(null,"Thank you, "+ card.getAccountHolderName()+ "\nfor using TOP BANK ATM.", "Logout successfully.",JOptionPane.INFORMATION_MESSAGE);
                    updateFile(card, filename);//updating the file using updateFile method
                    System.exit(0);//quit programm
            } 
            else{
                answer=0;
            }
        } 
    }
    
     /**checkCard method to verify that user input corresponds to any file name
     * 
     * @param Card
     * @return
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static boolean checkCard(String Card) throws FileNotFoundException,IOException{
        int notfound=0;
        boolean found;
        
        try(FileReader freader = new FileReader(Card)){
        }catch (FileNotFoundException e){
            JOptionPane.showMessageDialog(null,"Card not found!","ERROR", JOptionPane.WARNING_MESSAGE);
            notfound=1;
        }
        
        found = notfound != 1;
        
        return found;
    }
    
    /**Method to print all information modified or not into the file.
     * 
     * @param card
     * @param filename
     * @throws IOException 
     */
    public static void updateFile(Card card, String filename) throws IOException{
        FileWriter fwriter= new FileWriter(filename);

        try (PrintWriter outputFile = new PrintWriter(fwriter)) {
                outputFile.println(card.getPIN());
                outputFile.println(card.getAccountHolderName());
                outputFile.println(card.getBalance());
                outputFile.println(card.getWithdrawalLimit());
                outputFile.println(card.getAccountNumber());     
        }
    } 
}