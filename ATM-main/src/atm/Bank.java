package atm;

/**
 *
 * @author 1796128  - Roberto Di Biase - ATM project
 */
public class Bank {
    private String bankName;
    
    public Bank(String bankName) {
        this.bankName = bankName;
    }

    public String getBankName() {
        return bankName;
    }
}