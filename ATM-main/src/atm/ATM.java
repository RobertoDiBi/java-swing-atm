package atm;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author 1796128  - Roberto Di Biase - ATM project
 */
public class ATM extends Bank{

    public ATM(String bankName) {
        super(bankName);
    }
    
    /**withdrawal provides a cash withdrawal option. 
     * Client will be prompted with the following amounts to choose from: 20$ 60$ 100$ and 200$.
     * Verifies input with balance and withdrawal limit.
     * 
     * @param card
     */   
    public void withdrawal(Card card){

        int withdrawalScreen, amount;
        int withdrawalLimit;
        double balance;
        String samount;

        balance = card.getBalance();
        withdrawalLimit = card.getWithdrawalLimit();
        boolean check=false;
        if(!card.checkBalance()){
            JOptionPane.showMessageDialog(null, "Sorry your balance is: $"+balance,"ERROR",JOptionPane.WARNING_MESSAGE);
            check=true;
        }

        //Loop to ensure that the amount choose can be withdrawal and it is not over the withdrawal limit
        while(!check){
        Object[] options = {"Another Amount","200$","100$","60$","20$"};
                withdrawalScreen= JOptionPane.showOptionDialog(null,
                "Select ammount.",
                "Withdrawal",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options);

        switch(withdrawalScreen){
            case 4:
                if(20>withdrawalLimit || 20>balance){
                    JOptionPane.showMessageDialog(null,"The amount selected is over the withdrawal limit or your balance.\nAccount balance: $"+ balance+
                                                                            "\nWithdrawal limit: $"+withdrawalLimit+"\nPlease select another amount.","Error",JOptionPane.WARNING_MESSAGE);
                    check=false;
                }
                else{
                    balance -= 20;
                    card.setBalance(balance);
                    JOptionPane.showMessageDialog(null,"Operation compleated successfully.\nWithdrawal: $20","Withdrawal", JOptionPane.INFORMATION_MESSAGE);
                    check=true;
                    printReceipt(card);
                }
            break;
            case 3:
                if (60>withdrawalLimit || 60>balance){
                    JOptionPane.showMessageDialog(null,"The amount selected is over the withdrawal limit or your balance.\nAccount balance: $"+ balance+
                                                                            "\nWithdrawal limit: $"+withdrawalLimit+"\nPlease select another amount.","Error",JOptionPane.WARNING_MESSAGE);
                    check=false;
                }
                else{
                    balance -= 60;
                    card.setBalance(balance);
                    JOptionPane.showMessageDialog(null,"Operation compleated successfully.\nWithdrawal: $60","Withdrawal", JOptionPane.INFORMATION_MESSAGE);
                    check=true;
                    printReceipt(card);
                }
            break;
            case 2:
                if(100>withdrawalLimit || 100>balance){
                    JOptionPane.showMessageDialog(null,"The amount selected is over the withdrawal limit or your balance.\nAccount balance: $"+ balance+
                                                                            "\nWithdrawal limit: $"+withdrawalLimit+"\nPlease select another amount.","Error",JOptionPane.WARNING_MESSAGE);
                    check=false;
                }
                else{
                    balance -= 100;
                    card.setBalance(balance);
                    JOptionPane.showMessageDialog(null,"Operation compleated successfully.\nWithdrawal: $100","Withdrawal", JOptionPane.INFORMATION_MESSAGE);
                    check=true;
                    printReceipt(card);
                }
            break;
            case 1:
                if (200>withdrawalLimit || 200>balance){
                    JOptionPane.showMessageDialog(null,"The amount selected is over the withdrawal limit or your balance.\nAccount balance: $"+ balance+
                                                                            "\nWithdrawal limit: $"+withdrawalLimit+"\nPlease select another amount.","Error",JOptionPane.WARNING_MESSAGE);
                    check=false;
                }
                else{
                    balance -= 200;
                    card.setBalance(balance);
                    JOptionPane.showMessageDialog(null,"Operation compleated successfully.\nWithdrawal: $200","Withdrawal", JOptionPane.INFORMATION_MESSAGE);
                    check=true;
                    printReceipt(card);
                }
            break;
            case 0:
                boolean numformat;
                do{
                    try{
                        amount=Integer.parseInt(JOptionPane.showInputDialog("Please enter another amount."));

                        while(amount>withdrawalLimit || amount>balance)
                        {
                            amount=Integer.parseInt(JOptionPane.showInputDialog(null,"The amount entered is over the withdrawal limit or your balance.\nAccount balance: $"+ balance+
                                                                                                                        "\nWithdrawal limit: $"+withdrawalLimit+"\nPlease enter another amount.","Error",JOptionPane.WARNING_MESSAGE));
                        }
                        if ((amount%20)==0){
                            balance -= amount;
                            DecimalFormat df = new DecimalFormat("#.00");
                            samount= df.format(amount);
                            card.setBalance(balance);
                            JOptionPane.showMessageDialog(null,"Operation compleated successfully.\nWithdrawal: $"+samount,"Withdrawal", JOptionPane.INFORMATION_MESSAGE);
                            printReceipt(card);
                            check=true;
                            numformat=true;
                        }
                        else{
                            JOptionPane.showMessageDialog(null,"The ATM has only 20$ bills.\nThe amount should be multiple of 20.","Incorrect Input",JOptionPane.ERROR_MESSAGE);
                            numformat=false;
                        }
                    }catch (NumberFormatException nfe){
                        JOptionPane.showMessageDialog(null,"Can't withdrawal coins!","Incorrect Input",JOptionPane.ERROR_MESSAGE);
                        numformat=false;
                    }
                }while(!numformat);
            break;
            }
        }
    }
    
    /**deposit provides cash deposit options. The client should enter the amount to add to the account balance. 
     * The amount cannot accept change of coins only bills of (only 5,10,20,50or 100$ bills)
     * 
     * @param card
     */
    public void deposit(Card card){
        int amount;
        boolean checkAmount;
        double balance = card.getBalance();
        //Loop to ensure that the amount insert does not contai coins and it is only in 5, 10 , 20, 50, and 100 $ bills.
        do{
            try{
                amount=Integer.parseInt(JOptionPane.showInputDialog("Insert the amount you want to deposit."));
                if ((amount%100==0)||(amount%50==0)||(amount%20==0)||(amount%10==0)||(amount%5==0)){
                    balance+=amount;
                    card.setBalance(balance);
                    JOptionPane.showMessageDialog(null,"Operation compleated successfully.\nDeposit: $"+amount,"Deposit", JOptionPane.INFORMATION_MESSAGE);
                    checkAmount=true;
                }
                else{
                    JOptionPane.showMessageDialog(null, "You can't deposit coins!\nOnly 5$, 10$, 20$, 50$, and 100$ bills.","Incorrect input",JOptionPane.WARNING_MESSAGE);
                    checkAmount=false;
                }
            }catch (NumberFormatException nfe){
                JOptionPane.showMessageDialog(null, "You can't deposit coins!\nPlease enter only numbers without decimals!","Incorrect input",JOptionPane.WARNING_MESSAGE);
                checkAmount=false;
            }
        }while(!checkAmount);
    }
    
    /**Method to change the PIN number.
     * 
     * @param card
     */
    public void newPIN(Card card){
        int newPIN=0,confrmPIN;
        String tempPIN;
        boolean pinCheck,
                cancel=false;

        //loop to check that the new pin does not correspond to the actual pin and that is at least 4 digits
        do{
            try{
                tempPIN=(JOptionPane.showInputDialog("Please enter new PIN"));
                if (tempPIN==null){
                    cancel=true;
                    break;
                }
                    
                newPIN= Integer.parseInt(tempPIN);
                tempPIN= String.valueOf(newPIN);
                if(newPIN==card.getPIN()){
                    JOptionPane.showMessageDialog(null,"Please the new PIN must be different from your actual PIN","Incorrect input",JOptionPane.WARNING_MESSAGE);
                    pinCheck=false;
                }
                else{    
                    while(tempPIN.length()<4){
                    tempPIN=(JOptionPane.showInputDialog("Please enter a least 4 digits."));
                    if (tempPIN==null){
                        cancel=true;
                        break;
                    }
                    newPIN = Integer.parseInt(tempPIN);
                    tempPIN= String.valueOf(newPIN);
                    }
                    pinCheck=true;
                }
            }catch (NumberFormatException nfe){
                JOptionPane.showMessageDialog(null,"Please enter only digits.","Incorrect imput",JOptionPane.ERROR_MESSAGE);
                pinCheck=false;
            }
        }while (!pinCheck); 

        //Loop to confirm the new Pin
        while(!cancel){
            do{
                try{
                        tempPIN=(JOptionPane.showInputDialog("Please confirm your new PIN"));
                        if (tempPIN==null){
                            cancel=true;
                            break;
                        }
                        confrmPIN= Integer.parseInt(tempPIN);
                        if (newPIN==confrmPIN){
                            JOptionPane.showMessageDialog(null,"Your PIN has been updated!");
                            card.setPIN(newPIN);
                            pinCheck=true;
                            cancel=true;
                        }
                        else{
                            JOptionPane.showMessageDialog(null, "New PIN and Confirm PIN don't match!","Error",JOptionPane.ERROR_MESSAGE);
                            pinCheck=false;
                        }
                }catch (NumberFormatException nfe){
                    JOptionPane.showMessageDialog(null,"Please enter only digits.","Incorrect imput",JOptionPane.ERROR_MESSAGE);
                    pinCheck=false;
                }
            }while (!pinCheck);
        }
    }
    
    /**Method that prints the Receipt after the user has made a deposit or a withdrawal
     * 
     * @param card 
     */   
    public void printReceipt(Card card){
            int answer;

            answer = JOptionPane.showConfirmDialog(null,"Do you want to print your receipt?","TOP BANK ATM",JOptionPane.YES_NO_OPTION);
            if (answer==0)
                JOptionPane.showMessageDialog(null,"\nAccount No: "+card.getAccountNumber()+"\n"+ card.getAccountHolderName()+"\nYour balance is: $" + card.getBalance());
    } 
}